#ifndef POLY_H
#define POLY_H

/*! \file poly.h
    \brief Header of the poly class

    Header of the poly class to store information about a polygon and to encapsulate function
    to convert from the WKT format and to optimized rendering.
*/

#include <string>

#define NO 0


/*! \class poly
    \brief A class for polygon representation

  A class to store information about a polygon and to encapsulate function
  to convert from the WKT format and to optimized rendering.
*/

class poly
{
    public:

        float* polyX;	    /*!< Array with X coordinates of the polygon corners. */
	float* polyY;	    /*!< Array with Y coordinates of the polygon corners. */
	float* constant;    /*!< Array used for speedup rendering. */
	float* multiple;    /*!< Array used for speedup rendering. */
	int polyCorners;    /*!< Number of corners */

	/**
	 * \fn poly
	 * \brief poly object constructor
	 *
	 * Constructs the poly object to represent a polygon from a WKT string.

	 * \param s a std::string containing the Well-Kwon-Text (WKT) description of a polygon.
	 */

	poly(std::string s);

	/**
	 * \fn poly
	 * \brief poly constructor
	 *
	 * Constructs the poly object to represent a polygon from the coordinates of the corners. This
	 * method was retrived from the webpage http://alienryderflex.com/polygon/
	 *
	 * \param x array with the x coordinates of the corners.
	 * \param y array with the x coordinates of the corners.
	 * \param number of distinct corners in the polygon.
	 */

	poly(float *x, float* y,int corners);

	/**
	 * \fn ~poly
	 * \brief poly destructor
	 *
	 * Delete alocated memory.
	 */

	~poly();

	/**
	 * \fn pointInPolygon
	 * \brief Search if a point is inside the polygon.
	 *
	 * Search if the point with coordinates (x,y) is inside or outside the polygon.
	 *
	 * \param x x coordinate of the point to be searched.
	 * \param y y coordinate of the point to be searched
	 */

	bool pointInPolygon(float x,float y);

    private:
	void init(float* x, float*y, int corners);

	/**
	 * \fn precalc_values
	 * \brief Pre-calculation rotines to speedup pointInPolygon search.
	 *
	 * Alocates the array constant and multiple used to speed up the pointInPolygon search (used to render (fill) the polygon ).
	 */

	void precalc_values();
};

#endif // POLY_H
