/*! \file main.cpp

    \brief Main file of the project

    Read the Well-Known-Text (WKT) file with the polygons and
    it convert it to a pixelized image. If the command line argument
    is set, it will sabe the file in PNG and/or in VTK.
*/

#include <iostream>
#include <fstream>
#include <string>
#include "boost/program_options.hpp"
#include <Magick++.h>
#include <stdlib.h>

#include "poly.h"
#include "pixelized.h"

using namespace std;
using namespace Magick;

/*! \class stPolyOptions
    \brief Object to parse and store the comand line arguments.
*/

class stPolyOptions
{
    public:

        /*! \fn  stPolyOptions(int argc, char** argv)
	    \brief Constructor of the stPolyOptions class

	    Constructs the stPolyOtions by parsing the arguments described in (argc,argv)

	    \param argc the argc parameter of the main function
	    \param argc the argv parameter of the main function
	*/

    stPolyOptions(int argc, char** argv);
	vector<float> region;   /**< vector with the positions of the left,right,bottom,top, i.e., (minX,maxX,minY,maxY) (The units will be the same used for the polygons to be inserted) */
	vector<int> res;        /**< vetor with the resolution in x and y of the pixelized image. */
	string input;           /**< Name of the WKT (Well-Known-Text) to be converted to pixels */
	string outputVTK;       /**< PNG filename where the image should be saved. */
	string outputPNG;       /**< VTK filename where the image should be saved. */
};

int main(int argc, char** argv)
{   
    InitializeMagick(*argv);

    stPolyOptions opt(argc,argv);

    pixelized image(&opt.region[0],&opt.res[0]);

    ifstream filewkt( opt.input.c_str() );

    if (!filewkt.good())
    {
	cerr << "File " << opt.input << "not found!!!!" << endl;
	exit (EXIT_FAILURE);
    }

    string line;

    while ( getline(filewkt,line) )
    {
	poly p( line );
	image.insertPolygon( &p );
    }

    if (opt.outputVTK != "") image.writeToVTK( opt.outputVTK );
    if (opt.outputPNG != "") image.writeToPNG( opt.outputPNG );
    filewkt.close();

    return 0;
}

stPolyOptions::stPolyOptions(int argc, char** argv)
{
    using namespace boost::program_options;
    options_description desc("Allowed options");

    desc.add_options()
    ("help,h", "produce help message")
    ("input,i"  , value<string>(&input), "WKT filename to be converted to VTK")
    ("vtk"  , value<string>(&outputVTK) ->default_value(""), "VTK output filename")
    ("png"  , value<string>(&outputPNG) ->default_value(""), "PNG output filename")
    ("region,r"    , value< vector<float> >(&region) ->multitoken(), "Region of the WKT geometry to be converted to VTK. Ex: -r MinX MaxX MinY MaxY")
    ("size,s"      , value< vector<int> >(&res) ->multitoken(), "Size in pixel of the converted VTK. Ex: ResX, ResY ");

    variables_map vm;
    store( parse_command_line(argc, argv, desc), vm );
    notify(vm);

    if ( vm.count("help")  )
    {
	    cout << endl << desc << endl;
	    exit(0);
    }
}






