#ifndef PIXELIZED_H
#define PIXELIZED_H

/*! \file pixelized.h

    \brief Header for file for the class pixelized

    Header for file for the class pixelized to store a pixelized version of
    polygons and to save this pixelized version in different formats
*/

#include <string>

class poly; // Foward declaration

/*! \class pixelized
    \brief Class to represent (store) a pixelized version of polygons

    Class to store a pixelized version of
    polygons and to save this pixelized version in different formats like VTK and PNG
*/

class pixelized
{
    public:

        float minX;  /**< Position of the left  edge of the image. The units will be the same used for the polygons to be inserted */
	float maxX;  /**< Position of the right edge of the image. The units will be the same used for the polygons to be inserted */
	float minY;  /**< Position of the bottom edge of the image. The units will be the same used for the polygons to be inserted */
	float maxY;  /**< Position of the top edge of the image. The units will be the same used for the polygons to be inserted */

	int resolutionX;  /**< Number of pixels used in the X axis. */
	int resolutionY;  /**< Number of pixels used in the Y axis. */

	/*! \fn pixelized(float minX,float maxX,float minY,float maxY, int resX, int resY)
	    \brief Constructor of the pixelized class

	    Construng a pixelized class to represent a region located between (minX,maxX) and (minY,maxY) with a pixel representation
	    of the size (resX,resY).

	    \param minX Position of the left  edge of the image. The units will be the same used for the polygons to be inserted
	    \param maxX Position of the right edge of the image. The units will be the same used for the polygons to be inserted
	    \param minY Position of the bottom edge of the image. The units will be the same used for the polygons to be inserted
	    \param maxY Position of the top edge of the image. The units will be the same used for the polygons to be inserted
	    \param resX Number of pixels used in the X axis.
	    \param resY Number of pixels used in the Y axis
	*/

	pixelized(float minX,float maxX,float minY,float maxY, int resX, int resY);

	/*! \fn pixelized(float* size,int* res)
	    \brief Constructor of the pixelized class

	    Construng a pixelized class to represent a region located between (minX,maxX) and (minY,maxY) with a pixel representation
	    of the size (resX,resY).

	    \param size array with the positions of the left,right,bottom,top, i.e., (minX,maxX,minY,maxY) (The units will be the same used for the polygons to be inserted)
	    \param res  array with the resolution in x and y of the pixelized image.
	*/

	pixelized(float* size,int* res);

	/*! \fn  insertPolygon(poly *p)

	    \brief Insert a Polygon (poly class object) in the pixelized image

	    Insert a Polygon (poly class object) in the pixelized image. The function will render the polygon and stored in internal matrix of the pixelized class.

	    \param p poly class object with the polygon to be inserter/rendered.
	*/

	void insertPolygon(poly *p);

	/*! \fn  writeToVTK(std::string filename)

	    \brief Write the image stored in the object to the a VTK file.

	    Write the image stored in the object to the a VTK file. IMPORTANT: In the VTK file the value 1 is used if the center of the pixel does not intercept a polygon and 0 if it intercepts a polygon, i. e.,
	    when saving in the VTK it inverts the value from the internal matrix.

	    \param filename VTK filename.
	*/

	void writeToVTK(std::string filename);

	/*! \fn writeToPNG(std::string filename)

	    \brief Save the pixelized represantion to a PNG Image file.

	    Write the image stored in the object to the a PNG Image file. In the PNG file the pixel is assigned the with with color {255,255,255) if its  center does not intercept a polygon and the color black {0,0,0} if it intercepts a polygon.

	    \param filename PNG filename.
	*/

	void writeToPNG(std::string filename);

    private:

	/*! \fn init(float minX,float maxX,float minY,float maxY, int resX, int resY)

	    \brief Auxiliar function of the class's constructor.

	    This function will store the information about the geometry and it will alocate the memory needed to store the image.

	    \param minX Position of the left  edge of the image. The units will be the same used for the polygons to be inserted
	    \param maxX Position of the right edge of the image. The units will be the same used for the polygons to be inserted
	    \param minY Position of the bottom edge of the image. The units will be the same used for the polygons to be inserted
	    \param maxY Position of the top edge of the image. The units will be the same used for the polygons to be inserted
	    \param resX Number of pixels used in the X axis.
	    \param resY Number of pixels used in the Y axis
	*/

	void init(float minX,float maxX,float minY,float maxY, int resX, int resY);

	short* M;  /**< array with the pixelized image. The matrix will be a binary with the the value if the pixel is inside a polygon and 0 if is outsided.*/
	int numberOfPixels;  /**< total number of pixel in the image */
	float lenX;  /**< Length of the image in the x coordinates. The unit of this value is NOT in pixel but in the units used for the polygons */
	float lenY;  /**< Length of the image in the y coordinates. The unit of this value is NOT in pixel but in the units used for the polygons */
	float dx;  /** Separation between two pixels in x direction */
	float dy;  /** Separation between two pixels in y direction */
	float beginX;  /**< X position of the first pixel (at the left of the image) */
	float beginY;  /**< Y position of the first pixel (at the bottom of the image) */

};

#endif // PIXELIZED_H
