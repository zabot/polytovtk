#include <algorithm>
#include <string>
#include <Magick++.h>

#include "vtkio.h"
#include "pixelized.h"
#include "poly.h"

using namespace std;
using namespace Magick;

void pixelized::init(float minX,float maxX,float minY,float maxY, int resX, int resY)
{
    this -> minX = minX;
    this -> maxX = maxX;
    this -> minY = minY;
    this -> maxY = maxY;

    this -> lenX = maxX - minX;
    this -> lenY = maxY - minY;

    this -> resolutionX = resX;
    this -> resolutionY = resY;

    this -> M = new short[resolutionX*resolutionY] ();

    this -> dx = lenX / (resolutionX);
    this -> dy = lenY / (resolutionY);

    this -> beginX =  minX + dx*0.5;
    this -> beginY =  minY + dy*0.5;
    this -> numberOfPixels = resolutionX * resolutionY;
}

pixelized::pixelized(float minX,float maxX,float minY,float maxY, int resX, int resY)
{
    init(minX,maxX,minY,maxY,resX,resY);
}

pixelized::pixelized(float* size,int* res)
{
    init(size[0],size[1],size[2],size[3],res[0],res[1]);
}

void pixelized::insertPolygon(poly* p)
{
    int polyMinX = ( *min_element(p->polyX , p->polyX + p->polyCorners) - beginX )/dx + 1 ;
    int polyMaxX = ( *max_element(p->polyX , p->polyX + p->polyCorners) - beginX )/dx ;
    int polyMinY = ( *min_element(p->polyY , p->polyY + p->polyCorners) - beginY )/dy + 1;
    int polyMaxY = ( *max_element(p->polyY , p->polyY + p->polyCorners) - beginY )/dy ;

    for (int pixelY = max(0,polyMinY); pixelY <= min(polyMaxY,resolutionY-1); pixelY++)
    {
	int n = pixelY*resolutionX + polyMinX;
	for (int pixelX = max(0,polyMinX); pixelX <= min(polyMaxX,resolutionX-1) ; pixelX++)
	{
	    float y = beginY + pixelY * dy;
	    float x = beginX + pixelX * dx;
	    M[n] = ( M[n] || p -> pointInPolygon(x,y) );
	    n++;
	}
    }
}

void pixelized::writeToVTK(string filename)
{
    ofstream file( filename.c_str() );
    stVtkHeader vtkHeader = setVtkHeader("pixelizedPolygons",  vtkBinary , "SCALARS" , "Geometry ", "short", resolutionX  , resolutionY, 1);
    writeVtkHeader(file, vtkHeader);
    for (int n = 0; n < numberOfPixels; n++) writeVtkShort( file, vtkHeader, !M[n]);
    file.close();
}

void pixelized::writeToPNG(string filename)
{
    Image image;
    char* RGB = new char[3*resolutionX*resolutionY];

    for (int n = 0; n < resolutionX*resolutionY; n++)
    {
	RGB[3*n]   = 255* (!M[n]);
	RGB[3*n+1] = 255* (!M[n]);
	RGB[3*n+2] = 255* (!M[n]);
    }

    image.read(resolutionX ,resolutionY, "RGB" ,CharPixel, (void*) RGB );
    image.write( filename.c_str() );
    delete RGB;
}
