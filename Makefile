CC=g++

CFLAGS+= -O2 -fopenmp -O2 -msse4.2 -fomit-frame-pointer -ffast-math -funroll-all-loops -mfpmath=sse -finline-limit=800 
LFLAGS+= -lz -lboost_program_options -lGraphicsMagick++


EXECUTABLE=poly
SRC = poly.cpp vtkio.cpp pixelized.cpp main.cpp
OBJ = poly.o vtkio.o pixelized.o main.o

all : $(EXECUTABLE)

$(EXECUTABLE): $(OBJ)
	$(CC) $(OBJ) $(CFLAGS) $(LFLAGS) -o $(EXECUTABLE)

poly.o: poly.cpp poly.h
	g++ -c poly.cpp $(CFLAGS)

pixelized.o: pixelized.cpp pixelized.h vtkio.h poly.h
	g++ -c pixelized.cpp $(CFLAGS)

vtkio.o: vtkio.cpp vtkio.h
	g++ -c vtkio.cpp $(CFLAGS)

main.o: main.cpp poly.h pixelized.h
	g++ -c main.cpp $(CFLAGS)


debug: CFLAGS = -DDEBUG -g
debug: LFLAGS += -DDEBUG -g
debug: $(EXECUTABLE)

clean:
	rm *.o *~ p1 -f

