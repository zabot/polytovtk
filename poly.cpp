#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <string>
#include <vector>

#include "poly.h"

using namespace boost::geometry;
using namespace model;
using namespace std;

poly::poly(string wkt)
{
    typedef d2::point_xy<float> point_type;

    polygon<point_type> c;
    read_wkt( wkt , c);
    std::vector<point_type> const& points = c.outer();

    float* pointsX = new float[ points.size() ];
    float* pointsY = new float[ points.size() ];

    for (unsigned int n = 0; n < points.size() ; n++)
    {
	pointsX[n] =  get<0>(points[n]);
	pointsY[n] =  get<1>(points[n]);
    }

    this -> init( pointsX, pointsY ,points.size()-1);
}

poly::poly(float* x, float*y, int corners)
{
    init(x,y,corners);
}

void poly::init(float* x, float*y, int corners)
{
        this -> polyX = x;
        this -> polyY = y;

        this -> polyCorners = corners;

        this -> constant = new float[corners];
        this -> multiple = new float[corners];
        this -> precalc_values();
}

poly::~poly()
{
    delete polyX;
    delete polyY;
    delete constant;
    delete multiple;
}

//  Globals which should be set before calling these functions:
//
//  int    polyCorners  =  how many corners the polygon has (no repeats)
//  float  polyX[]      =  horizontal coordinates of corners
//  float  polyY[]      =  vertical coordinates of corners
//  float  x, y         =  point to be tested
//
//  The following global arrays should be allocated before calling these functions:
//
//  float  constant[] = storage for precalculated constants (same size as polyX)
//  float  multiple[] = storage for precalculated multipliers (same size as polyX)
//
//  (Globals are used in this example for purposes of speed.  Change as
//  desired.)
//
//  USAGE:
//  Call precalc_values() to initialize the constant[] and multiple[] arrays,
//  then call pointInPolygon(x, y) to determine if the point is in the polygon.
//
//  The function will return YES if the point x,y is inside the polygon, or
//  NO if it is not.  If the point is exactly on the edge of the polygon,
//  then the function may return YES or NO.
//
//  Note that division by zero is avoided because the division is protected
//  by the "if" clause which surrounds it.

void poly::precalc_values()
{
    int   i, j=polyCorners-1 ;

    for(i=0; i<polyCorners; i++)
    {
	if(polyY[j]==polyY[i])
	{
	    constant[i]=polyX[i];
	    multiple[i]=0;
	}
	else
	{
	    constant[i]=polyX[i]-(polyY[i]*polyX[j])/(polyY[j]-polyY[i])+(polyY[i]*polyX[i])/(polyY[j]-polyY[i]);
	    multiple[i]=(polyX[j]-polyX[i])/(polyY[j]-polyY[i]);
	}
	j=i;
    }
}

bool poly::pointInPolygon(float x,float y)
{
    int   i, j=polyCorners-1 ;
    bool  oddNodes=NO        ;

    for (i=0; i<polyCorners; i++)
    {
	if ((polyY[i]< y && polyY[j]>=y  ||   polyY[j]< y && polyY[i]>=y))
	{
	    oddNodes^=(y*multiple[i]+constant[i]<x);
	}
	j=i;
    }

    return oddNodes;
}
